import 'dart:io';

bool isLoggedToday (d) => DateTime.now().day == d['time'].day && DateTime.now().month == d['time'].month;

List getTimeEntriesByDate(DateTime datetime, List time_entries) {
  bool isLoggedOnDate (d) =>  datetime.day == d['time'].day && datetime.month == d['time'].month;
  return time_entries.where(isLoggedOnDate).toList();
}

Duration getTimeDiff(DateTime start, DateTime end) {
  return end.difference(start);
}

DateTime parseDateTime(String datetime_str) {
  var parts = datetime_str.split('h');
  int hour = int.parse(parts[0]);
  int minutes = int.parse(parts[1]);
  DateTime now = DateTime.now();
  return DateTime(now.year, now.month, now.day, hour, minutes);
}

Duration getTotalTime(List time_entries, DateTime datetime) {
  List date_entries = getTimeEntriesByDate(datetime, time_entries);
  date_entries.forEach(
    (item) => print('logged entry: (${item['direction']})\t${item['time'].toString()}')
  );
  //var today_entries = time_entries.where(isLoggedToday).toList();
  DateTime end_time = DateTime.now();
  Duration total_time = Duration();
  for (Map time_entry in date_entries.reversed) {
    String direction = time_entry['direction'];
    DateTime logged_time = time_entry['time'];
    if (direction == 'out') {
      end_time = logged_time;
      continue;
    }
    total_time += getTimeDiff(logged_time, end_time);
  }
  return total_time;
}

Map getTimeEntry() {
  stdout.write('Enter time entry: ');
  String time_str = stdin.readLineSync();
  DateTime time = parseDateTime(time_str);
  stdout.write('Enter entry direction: ');
  String direction = stdin.readLineSync();
  
  var time_entry = {
    'direction': direction,
    'time': time
  };

  return time_entry;
}

getWorkingDays(String starting_date_str) {
  var starting_date = DateTime.parse('2019-10-28');
  List<DateTime> working_days = new List();
  working_days.add(starting_date);
  for (var i = 1; i < 5; i++) {
    var next_date = starting_date.add(Duration(days: i));
    working_days.add(next_date);
  }
  return working_days;
}

printStats(time_entries) {
  var starting_date = DateTime.parse('2019-11-11');
  List<DateTime> logged_dates = new List();
  logged_dates.add(starting_date);
  for (var i = 1; i < 5; i++) {
    var next_date = starting_date.add(Duration(days: i));
    logged_dates.add(next_date);
  }
  var total_duration = new Duration();
  var hours = 0;
  var minutes = 0;
  for (var datetime in logged_dates) {
    Duration total_time = getTotalTime(time_entries, datetime);
    hours = total_time.inHours;
    minutes = total_time.inMinutes % 60;
    print('$datetime : $hours hours and $minutes minutes');
    total_duration += total_time;
  }
  hours = total_duration.inHours;
  minutes = total_duration.inMinutes % 60;
  print('========== Total work duration for the week: $hours hours and $minutes minutes [Expected 35 hours].');
  var time_left = Duration(hours: 35) - total_duration;
  hours = time_left.inHours;
  minutes = time_left.inMinutes % 60;
  print('========== Time left to meet week expected workload: $hours hours and $minutes minutes');
  DateTime time_to_leave = DateTime.now().add(time_left);
  print('========== Expected time to leave is: ${time_to_leave.hour}:${time_to_leave.minute}\n');

}

printTimeTotal(time_entries, DateTime datetime) {
  Duration total_time = getTotalTime(time_entries, datetime);
  var hours = total_time.inHours;
  var minutes = total_time.inMinutes % 60;
  print('\n========== You have worked for $hours hours and $minutes minutes.'); 
  Duration time_remaining = Duration(hours:7) - total_time;
  hours = time_remaining.inHours;
  minutes = time_remaining.inMinutes % 60;
  print('========== You have $hours hours and $minutes minutes left.');
  DateTime time_to_leave = DateTime.now().add(time_remaining);
  print('========== Expected time to leave is: ${time_to_leave.hour}:${time_to_leave.minute}\n');
}

int getUserInput() {
  print('[1] Add time entry');
  print('[2] Display today total time');
  print('[3] Display date total time');
  print('[4] Display stats');
  print('[0] Exit');
  stdout.write('Please selection an action: ');
  String selection = stdin.readLineSync();
  return int.parse(selection);
}

DateTime getDateTime() {
  stdout.write('Please enter a date: ');
  String date_str = stdin.readLineSync();
  print(date_str);
  return DateTime.parse(date_str);
}

Future getTimeEntriesFromDisk(String path) async {
  var time_entries = List();
  var logFile = File(path);
  var items;

  items = await logFile.readAsLines();
  for (var item in items) {
    var time_entry = {
      'direction': item.split('#')[0],
      'time': DateTime.parse(item.split('#')[1])
    };
    time_entries.add(time_entry);
  }
  
  return time_entries;
}

Future saveTimeEntriesToDisk(String path, List time_entries) async {
  var logFile = File(path);
  var sink = logFile.openWrite(mode: FileMode.append);
  for (var time_entry in time_entries) {
    sink.write('${time_entry['direction']}#${time_entry['time']}\n');
  }
  await sink.flush();
  await sink.close();
}

Future logTimeEntry(String path, Map time_entry) async {
  var logFile = File(path);
  var sink = logFile.openWrite(mode: FileMode.append);
  sink.write('${time_entry['direction']}#${time_entry['time']}\n');
  await sink.flush();
  await sink.close();
}

Future main()  async {
  var path = 'task_logger.log';
  var time_entries = await getTimeEntriesFromDisk(path);
  int selection = getUserInput();
  while(selection != 0) {
    if(selection == 1) {
      Map time_entry = getTimeEntry();
      time_entries.add(time_entry);
      logTimeEntry(path, time_entry);
    } else if (selection == 2) {
      printTimeTotal(time_entries, DateTime.now());
    } else if (selection == 3) {
      DateTime datetime = getDateTime();
      printTimeTotal(time_entries, datetime);
    } else if (selection == 4) {
      print('\n');
      printStats(time_entries);
      print('\n');
    }
    selection = getUserInput();
  }
}
